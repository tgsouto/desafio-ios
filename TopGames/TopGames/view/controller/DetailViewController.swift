//
//  DetailViewController.swift
//  TopGames
//
//  Created by Tiago Souto on 04/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var box: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var viewers: UILabel!
    
    var game: GameModel.Game = GameModel.Game()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.text = self.game.name
        
        let viewersFormatted = FormatUtil.formatNumber(number: self.game.viewers)
        self.viewers.text = viewersFormatted!
        
        if let imageURL = URL(string: self.game.box!.large) {
            self.box.af_setImage(withURL: imageURL) //set image automatically when download compelete.
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
