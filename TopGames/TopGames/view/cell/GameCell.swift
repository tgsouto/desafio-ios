//
//  GameCell.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class GameCell: UITableViewCell {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ranking: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setModel (game: GameModel.Game, rank: Int) {
        self.name.text = game.name
        let rankString: String = "#" + String(rank.description)
        self.ranking.text = rankString
        
        if let imageURL = URL(string: game.logo!.medium) {
            self.logo.af_setImage(withURL: imageURL) //set image automatically when download compelete.
        }
    }

}
