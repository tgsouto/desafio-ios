//
//  MImageRounded.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import UIKit

@IBDesignable
class MImageRounded: UIImageView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white
    @IBInspectable var borderWidth: CGFloat = 0
    
    override func layoutSubviews() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderWidth = self.borderWidth
        self.layer.borderColor = self.borderColor.cgColor
    }
}
