//
//  MainViewModelFactory.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation

struct MainViewModelFactory {
    static func create (appDelegate: AppDelegate) -> MainViewModel {
        let context = appDelegate.persistentContainer.viewContext
        let gameRepository = GamesRepository(gamesRemote: GamesRemote(), gameDao: GameDAO(context: context))
        return MainViewModel(gameRepository: gameRepository)
    }
}
