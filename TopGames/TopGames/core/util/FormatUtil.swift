//
//  FormatUtil.swift
//  TopGames
//
//  Created by Fagron Technologies on 04/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation

struct FormatUtil {
    static func formatNumber (number: Int) -> String? {
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(integerLiteral: number))
    }
}
