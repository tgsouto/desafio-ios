//
//  MainViewModel.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import RxSwift

class MainViewModel {
    
    private var gameRepository: GamesRepository
    private var next: Int = 0
    private var itemsPerPage: Int = 100
    var loading: Bool = false
    var ended: Bool = false
    
    init (gameRepository: GamesRepository) {
        self.gameRepository = gameRepository
    }
    
    func listGames (clearCache: Bool = false) -> Observable<[GameModel.Game]> {
        self.loading = true
        if clearCache {
            self.next = 0
        }
        return Observable.create { observer in
            self.gameRepository.getTopGames(next: self.next, itemsPerPage: self.itemsPerPage, clearCache: clearCache)
                .subscribe(onNext: { games in
                    self.next += self.itemsPerPage
                    if self.next > games.total {
                        self.ended = true
                    }
                    self.loading = false
                    observer.on(.next(games.games))
                    observer.on(.completed)
                })
        }
    }
}
