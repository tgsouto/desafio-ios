//
//  GameDAO.swift
//  TopGames
//
//  Created by Fagron Technologies on 04/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class GameDAO {
    
    private let ENTITY = "Game"
    private var game: Game
    private var context: NSManagedObjectContext
    
    init (context: NSManagedObjectContext) {
        self.context = context
        self.game = Game(context: context)
    }
    
    func getContext() -> NSManagedObjectContext {
        return context
    }
    
    func setGame (game: Game) {
        self.game = game
    }
    
    func add () -> Bool {
        var saved = true
        if self.game.name != nil {
            do {
                try self.context.save()
            } catch {
                saved = false
            }
        } else {
            saved = false
        }
        return saved
    }

    func list (next: Int, itemsPerPage: Int) -> [Game] {
        var games: [Game]
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.ENTITY)
        request.fetchLimit = itemsPerPage
        request.fetchOffset = next
        
        let order = NSSortDescriptor(key: "popularity", ascending: true)
        request.sortDescriptors = [order]
        
        let predicade = NSPredicate(format: "name <> nil")
        request.predicate = predicade
        do {
            games = try self.context.fetch(request) as! [Game]
        } catch {
            games = []
        }
        debugPrint(games)
        return games
    }
    
    func countAll () -> Int {
        var count: Int
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.ENTITY)
        
        let predicade = NSPredicate(format: "name <> nil")
        request.predicate = predicade
        do {
            count = try self.context.count(for: request)
        } catch {
            count = 0
        }
        if count == NSNotFound { count = 0 }
        debugPrint(count)
        return count
    }
    
    func deleteAll () -> Bool {
        var saved = true
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: self.ENTITY)
        let batchDelete = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            try self.context.execute(batchDelete)
        } catch {
            saved = false
        }
        return saved
    }
}
