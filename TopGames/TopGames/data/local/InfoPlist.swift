//
//  InfoPlist.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation

class InfoPlist  {
    private var infos: Dictionary<String, AnyObject>
    init () {
        if let infosPlist = Bundle.main.infoDictionary {
            self.infos = infosPlist as Dictionary<String, AnyObject>
        } else {
            self.infos = [:]
        }
    }
    
    func getValue (key: String) -> AnyObject {
        return infos[key]!
    }
    
    var clientId: String {
        get {
            return self.getValue(key: "ClientID") as! String
        }
    }
    
    var urlApi: String {
        get {
            return self.getValue(key: "UrlAPI") as! String
        }
    }
}
