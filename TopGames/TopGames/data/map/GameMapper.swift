//
//  GameMapper.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import CoreData

struct GameMapper {
    
    /*
     Converte o modelo usado para exibição na classe que será usada para salvar no banco de dados
     */
    static func toGameList (topGames: GameModel.TopGames, context: NSManagedObjectContext) -> [Game] {
        var games: [Game] = []
        
        for (index, gameModel) in topGames.games.enumerated() {
            let game = Game(context: context)
            game.id = Int32(gameModel.id)
            game.name = gameModel.name
            game.popularity = Int64(gameModel.popularity)
            game.viewers = Int32(gameModel.viewers)
            game.channels = Int32(gameModel.channels)
            game.order = Int16(index)
            
            let box = gameModel.box!
            let boxLarge = GameImage(context: context)
            boxLarge.gameId = Int32(gameModel.id)
            boxLarge.url = box.large
            boxLarge.typeEnum = .box
            boxLarge.sizeEnum = .large
            game.addToGameImage(boxLarge)
            
            let boxMedium = GameImage(context: context)
            boxMedium.gameId = Int32(gameModel.id)
            boxMedium.url = box.medium
            boxMedium.typeEnum = .box
            boxMedium.sizeEnum = .medium
            game.addToGameImage(boxMedium)
            
            let boxSmall = GameImage(context: context)
            boxSmall.gameId = Int32(gameModel.id)
            boxSmall.url = box.small
            boxSmall.typeEnum = .box
            boxSmall.sizeEnum = .small
            game.addToGameImage(boxSmall)
            
            let boxTemplate = GameImage(context: context)
            boxTemplate.gameId = Int32(gameModel.id)
            boxTemplate.url = box.template
            boxTemplate.typeEnum = .box
            boxTemplate.sizeEnum = .template
            game.addToGameImage(boxTemplate)
            
            let logo = gameModel.logo!
            let logoLarge = GameImage(context: context)
            logoLarge.gameId = Int32(gameModel.id)
            logoLarge.url = logo.large
            logoLarge.typeEnum = .logo
            logoLarge.sizeEnum = .large
            game.addToGameImage(logoLarge)
            
            let logoMedium = GameImage(context: context)
            logoMedium.gameId = Int32(gameModel.id)
            logoMedium.url = logo.medium
            logoMedium.typeEnum = .logo
            logoMedium.sizeEnum = .medium
            game.addToGameImage(logoMedium)
            
            let logoSmall = GameImage(context: context)
            logoSmall.gameId = Int32(gameModel.id)
            logoSmall.url = logo.small
            logoSmall.typeEnum = .logo
            logoSmall.sizeEnum = .small
            game.addToGameImage(logoSmall)
            
            let logoTemplate = GameImage(context: context)
            logoTemplate.gameId = Int32(gameModel.id)
            logoTemplate.url = logo.template
            logoTemplate.typeEnum = .logo
            logoTemplate.sizeEnum = .template
            game.addToGameImage(boxTemplate)
            
            games.append(game)
        }
        
        return games
    }
    
    static func fromGameList (games: [Game]) -> GameModel.TopGames {
        let topGames = GameModel.TopGames()
        for game in games {
            let gameModel = GameModel.Game()
            gameModel.id = Int(game.id)
            gameModel.name = game.name!
            gameModel.popularity = Int(game.popularity)
            gameModel.channels = Int(game.channels)
            gameModel.viewers = Int(game.viewers)
            let images = game.gameImage!.allObjects
            let boxs = images.filter { imgs in
                return (imgs as! GameImage).typeEnum == .box
            } as! [GameImage]
            for box in boxs {
                switch box.sizeEnum {
                case .large:
                    gameModel.box?.large = box.url!
                case .medium:
                    gameModel.box?.medium = box.url!
                case .small:
                    gameModel.box?.large = box.url!
                case .template:
                    gameModel.box?.template = box.url!
                }
            }
            
            let logos = images.filter { imgs in
                return (imgs as! GameImage).typeEnum == .logo
            } as! [GameImage]
            for logo in logos {
                switch logo.sizeEnum {
                case .large:
                    gameModel.box?.large = logo.url!
                case .medium:
                    gameModel.box?.medium = logo.url!
                case .small:
                    gameModel.box?.large = logo.url!
                case .template:
                    gameModel.box?.template = logo.url!
                }
            }
            topGames.games.append(gameModel)
        }
        
        return topGames
    }
    
    /*
     Converte o modelo de retorno da API par o modelo que será usado para exibir na tela
     */
    static func fromResponse (gameResponse: ResponseModel.TopGames) -> GameModel.TopGames {
        let topGames = GameModel.TopGames()
        
        topGames.total = gameResponse.total
        var games: [GameModel.Game] = []
        for rankingResponse in gameResponse.games {
            let game = self.fromRankingGamesResponse(rankingResponse: rankingResponse)
            games.append(game)
        }
        topGames.games = games
        
        return topGames
    }
    
    private static func fromRankingGamesResponse (rankingResponse: ResponseModel.RankingGame) -> GameModel.Game {
        let game = GameModel.Game()
        game.id = rankingResponse.game!.id
        game.name = rankingResponse.game!.name
        game.popularity = rankingResponse.game!.popularity
        game.channels = rankingResponse.channels
        game.viewers = rankingResponse.viewers
        game.box = self.fromGameImagesResponse(imagesResponse: rankingResponse.game!.box!)
        game.logo = self.fromGameImagesResponse(imagesResponse: rankingResponse.game!.logo!)
        return game
    }
    
    private static func fromGameImagesResponse (imagesResponse: ResponseModel.GameImages) -> GameModel.GameImages {
        let images = GameModel.GameImages()
        images.large = imagesResponse.large
        images.medium = imagesResponse.medium
        images.small = imagesResponse.small
        images.template = imagesResponse.template
        return images
    }
    
}
