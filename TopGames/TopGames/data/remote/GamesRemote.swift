//
//  GamesRemote.swift
//  TopGames
//
//  Created by Fagron Technologies on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RxSwift


class GamesRemote: BasicRemote {
    init() {
        super.init(useClientId: true)
    }
    
    func topGames (next: Int, itemsPerPage: Int) -> Observable<ResponseModel.TopGames?> {
        let url = self.getBaseUrl() + "games/top?limit=" + String(itemsPerPage.description) + "&offset=" + String(next.description)
        return Observable.create { observer in
            Alamofire.request(url, method: .get, headers: self.headers).responseObject {
                (response: DataResponse<ResponseModel.TopGames>) in
                debugPrint(response.result)
                if (response.result.isSuccess) {
                    let games: ResponseModel.TopGames = response.result.value!
                    observer.on(.next(games))
                } else {
                    observer.on(.next(nil))
                }
                observer.on(.completed)
            }
            return Disposables.create()
        }
    }
}
