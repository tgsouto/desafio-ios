//
//  BasicRemote.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import Alamofire

class BasicRemote {
    var headers: HTTPHeaders = [:]
    private var infoPlist: InfoPlist
    
    init(useClientId: Bool) {
        self.infoPlist = InfoPlist()
        if (useClientId) {
            self.headers["Client-ID"] = self.infoPlist.clientId
        }
    }
    
    func getBaseUrl (type: String = "") -> String {
        let baseUrl: String = self.infoPlist.urlApi
        return baseUrl
    }
}
