//
//  GamesModel.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation

struct GameModel {
    class Game {
        var id: Int = 0
        var name: String = ""
        var popularity: Int = 0
        var box: GameModel.GameImages? = GameModel.GameImages()
        var logo: GameModel.GameImages? = GameModel.GameImages()
        var viewers: Int = 0
        var channels: Int = 0
    }
    
    class GameImages {
        var large: String = ""
        var medium: String = ""
        var small: String = ""
        var template: String = ""
    }
    
    class TopGames {
        var games: [GameModel.Game] = []
        var total: Int = 0
    }
}
