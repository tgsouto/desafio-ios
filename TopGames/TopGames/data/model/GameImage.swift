//
//  GameImage.swift
//  TopGames
//
//  Created by Fagron Technologies on 04/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import CoreData

@objc(GameImage)
public class GameImage: NSManagedObject {
    var typeEnum: GameImageType {
        get { return GameImageType(rawValue: Int(self.type))! }
        set { self.type = Int16(newValue.rawValue) }
    }
    var sizeEnum: GameImageSize {
        get { return GameImageSize(rawValue: Int(self.size))! }
        set { self.size = Int16(newValue.rawValue) }
    }
}
