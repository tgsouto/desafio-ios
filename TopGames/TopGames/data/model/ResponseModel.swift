//
//  ResponseModel.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseModel {
    class Game: Mappable {
        var id: Int = 0
        var name: String = ""
        var popularity: Int = 0
        var box: ResponseModel.GameImages?
        var logo: ResponseModel.GameImages?

        required init?(map: Map) {}

        func mapping(map: Map) {
            self.id <- map["_id"]
            self.name <- map["name"]
            self.popularity <- map["popularity"]
            self.box <- map["box"]
            self.logo <- map["logo"]
        }
    }
    
    class GameImages: Mappable {
        var large: String = ""
        var medium: String = ""
        var small: String = ""
        var template: String = ""
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            self.large <- map["large"]
            self.medium <- map["medium"]
            self.small <- map["small"]
            self.template <- map["template"]
        }
    }
    
    class RankingGame: Mappable {
        var game: ResponseModel.Game?
        var viewers: Int = 0
        var channels: Int = 0

        required init?(map: Map) {}

        func mapping(map: Map) {
            self.game <- map["game"]
            self.viewers <- map["viewers"]
            self.channels <- map["channels"]
        }
    }
    
    class TopGames: Mappable {
        var games: [ResponseModel.RankingGame] = []
        var total: Int = 0

        required init?(map: Map) {}

        func mapping(map: Map) {
            self.games <- map["top"]
            self.total <- map["_total"]
        }

    }
}
