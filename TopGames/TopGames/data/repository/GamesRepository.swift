//
//  File.swift
//  TopGames
//
//  Created by Tiago Souto on 03/05/18.
//  Copyright © 2018 Tiago Souto. All rights reserved.
//

import Foundation
import RxSwift

class GamesRepository {
    
    private var gamesRemote: GamesRemote
    private var gameDao: GameDAO
    private var clearCache: Bool = false
    
    init(gamesRemote: GamesRemote, gameDao: GameDAO) {
        self.gamesRemote = gamesRemote
        self.gameDao = gameDao
    }
    
    func getTopGames (next: Int, itemsPerPage: Int, clearCache: Bool) -> Observable<GameModel.TopGames> {
        self.clearCache = clearCache
//        return self.getTopRemote(next: next, itemsPerPage: itemsPerPage)
//        self.deleteAll()
        return self.getTopLocal(next: next, itemsPerPage: itemsPerPage).flatMap { (games: GameModel.TopGames) -> Observable<GameModel.TopGames> in
            if Connectivity.isConnectedToInternet() && (games.games.count == 0 || self.clearCache) {
                return self.getTopRemote(next: next, itemsPerPage: itemsPerPage).flatMap { (gamesResponse: GameModel.TopGames) -> Observable<GameModel.TopGames> in
                    if clearCache {
                        self.deleteAll()
                        self.clearCache = false
                    }
                    self.addAll(topGames: gamesResponse)
                    return Observable.just(gamesResponse)
                }
            }

            return Observable.just(games)
        }
    }
    
    func add (game: Game) {
        self.gameDao.setGame(game: game)
        self.gameDao.add()
    }
    
    func addAll (topGames: GameModel.TopGames) {
        let games = GameMapper.toGameList(topGames: topGames, context: self.gameDao.getContext())
        for game in games {
            self.add(game: game)
        }
    }
    
    func deleteAll () {
        self.gameDao.deleteAll()
    }
    
    private func getTopRemote (next: Int, itemsPerPage: Int) -> Observable<GameModel.TopGames> {
        return Observable.create { observer in
            self.gamesRemote.topGames(next: next, itemsPerPage: itemsPerPage)
                .subscribe(onNext: { gamesResponse in
                    let gameModel = GameMapper.fromResponse(gameResponse: gamesResponse!)
                    observer.on(.next(gameModel))
                    observer.on(.completed)
                })
        }
    }
    
    private func getTopLocal (next: Int, itemsPerPage: Int) -> Observable<GameModel.TopGames> {
        return Observable.create { observer in
            let gamesMO = self.gameDao.list(next: next, itemsPerPage: itemsPerPage)
            let count = self.gameDao.countAll()
            let topGames = GameMapper.fromGameList(games: gamesMO)
            topGames.total = count
            observer.on(.next(topGames))
            observer.on(.completed)
            return Disposables.create()
        }
    }
    
}
